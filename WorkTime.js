'use strict';

var mongoose = require('mongoose');


class WorkTime{


    constructor() {

        var workTimeSchema = new mongoose.Schema({
            user_id: {type: Number, required: true},
            startTime: {type: Date, required: true},
            endTime: {type: Date}
        });

        this.workTime = mongoose.model('workTime', workTimeSchema);

    }

    addNew(user_id, cb) {
        var newRow = new this.workTime({
            user_id: user_id,
            startTime: new Date()
        });

        newRow.save(function (err, cb) {
            if (err) return console.error(err);

            newRow.state = 'open';
            newRow.save(cb);
        });
    }

    findActive(user_id) {
        var today = new Date();
        today.setHours(0,0,0,0);

        return this.workTime.find({user_id: user_id, startTime: {$gt: today}, endTime: undefined}).cursor();
    }

    closeActive(workTimeObj, endTime, cb){
        if (!endTime) {
            workTimeObj.endTime = new Date();
        } else {
            workTimeObj.endTime = endTime;
        }
        console.log(workTimeObj);
        workTimeObj.save(cb);
    }
}

module.exports = WorkTime;