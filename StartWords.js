'use strict';


const SimpleModel = require('./SimpleModel');

/**
 * @function add
 * @function remove
 */
class StartWords extends SimpleModel{

    constructor(){
        super({
            word: {type: String, unique: true, required: true, dropDups: true},
            user: {id: Number, name: String},
            updated_at: { type: Date, default: Date.now }
        }, 'words_start');
    }
}

module.exports = StartWords;