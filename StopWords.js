'use strict';

const SimpleModel = require('./SimpleModel');

class StopWords extends SimpleModel {

    constructor() {
        super({
            word: {type: String, unique: true, required: true, dropDups: true},
            user: {id: Number, name: String},
            updated_at: {type: Date, default: Date.now}
        }, 'words_stop');
    }

}

module.exports = StopWords;