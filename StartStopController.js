'use strict';

const TelegramBaseController = require('telegram-node-bot/lib/mvc/TelegramBaseController');

var StartWords = require('./StartWords');
var startWords = new StartWords();

var StopWords = require('./StopWords');
var stopWords = new StopWords();

class StartStopController extends TelegramBaseController {
    /**
     * @param {Scope} $
     */
    pingHandler($) {

        // $.sendMessage('Hello ' + $.message.from.id)
    }

    _removeCommandFromText(inText) {
        return inText.replace(/^\/\w+\s*(\S*)$/ui, '$1');
    }


    addStartWordHandler($) {
        var word = this._removeCommandFromText($.message.text);

        if (word.length > 0) {
            $.sendMessage(word);
            startWords.add({
                word: word,
                user: {
                    id: $.message.from.id,
                    name: $.message.from.firstName + ' ' + $.message.from.lastName
                }
            }, function (err) {
                if (err) console.log(err);
                $.sendMessage(word);
            });
        } else {
            console.log('empty start word');
        }

    }

    deleteStartWordHandler($) {
        var word = this._removeCommandFromText($.message.text);

        if (word.length > 0) {
            $.sendMessage(word);
            startWords.remove({
                word: word
            }, function (err) {
                if (err) console.log(err);
                $.sendMessage(word + ' - deleted');
            });
        } else {
            console.log('empty start word');
        }

    }

    addStopWordHandler($) {
        var word = this._removeCommandFromText($.message.text, 'addStopWord');
        if (word) {
            $.sendMessage(word);

            stopWords.add({
                word: word,
                user: {
                    id: $.message.from.id,
                    name: $.message.from.firstName + ' ' + $.message.from.lastName
                }
            }, function (err) {
                if (err) console.log(err);
                $.sendMessage('OK');
            });
        }
    }

    deleteStopWordHandler($) {
        var word = this._removeCommandFromText($.message.text);

        if (word.length > 0) {
            $.sendMessage(word);
            stopWords.remove({
                word: word
            }, function (err) {
                if (err) console.log(err);
                $.sendMessage(word + ' - deleted');
            });
        } else {
            console.log('empty start word');
        }

    }

    get routes() {
        return {
            '/ping': 'pingHandler',
            '/addStartWord': 'addStartWordHandler',
            '/addStopWord': 'addStopWordHandler',
            '/deleteStartWord' : 'deleteStartWordHandler',
            '/deleteStopWord' : 'deleteStopWordHandler'
        }
    }
}

module.exports = StartStopController;