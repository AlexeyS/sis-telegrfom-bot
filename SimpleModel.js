'use strict';

var mongoose = require('mongoose');

/**
 * @method add
 */
class simpleModel{
    constructor(schema, tableName) {

        var modelSchema = new mongoose.Schema(schema);

        this.model = mongoose.model(tableName, modelSchema);

    }

    add(data, cb) {
        var newRow = new this.model(data);

        newRow.save(function (err, cb) {
            if (err) return console.error(err);

            newRow.state = 'open';
            newRow.save(cb);
        });
    }

    remove(data, cb){
        this.model.remove(data, cb);
    }

    findActive(user_id) {
        var today = new Date();
        today.setHours(0,0,0,0);

        return this.workTime.find({user_id: user_id, startTime: {$gt: today}, endTime: undefined}).cursor();
    }

}

module.exports = simpleModel;